#include "file.h"


File::File(QString filePath):
	QFile(),QListWidgetItem()
{
	setFileName(filePath);
	QFileInfo fi(fileName());
	QString text = fi.baseName();
	setText(text);
	m_image = 0;
	setFlags(flags() | Qt::ItemIsEditable);
}

File::~File()
{

}

QPixmap File::image()
{
	QPixmap* img = new QPixmap(fileName());
	return img->scaled(600,800,Qt::KeepAspectRatio);
}

void File::saveWithNewName()
{
	QFileInfo fi(fileName());
	QString newName = fi.absolutePath() + "/" + text() + "." + fi.completeSuffix();
	rename(fileName(),newName);
}

