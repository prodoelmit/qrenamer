#ifndef FILE_H
#define FILE_H
#include <QListWidgetItem>
#include <QDir>
#include <QFile>
#include <QImage>
class File: public QListWidgetItem, public QFile
{
public:
	File(QString filePath);
	~File();
	QPixmap image();
	void saveWithNewName();
private:
	QImage* m_image;
};

#endif // FILE_H
