#include "mw.h"

#include <QLayout>
MW::MW(QWidget *parent) :
	QMainWindow(parent)
{
	QWidget* cWidget = new QWidget;
	QHBoxLayout* hcLayout = new QHBoxLayout;
	cWidget->setLayout(hcLayout);
	setCentralWidget(cWidget);
	m_imageLabel = new QLabel(this);
	m_listWidget = new QListWidget(this);

	hcLayout->addWidget(m_imageLabel);
	hcLayout->addWidget(m_listWidget);
//	hcLayout->addWidget(m_lineEdit);
	QShortcut* chooseDirectoryShortcut = new QShortcut(QKeySequence("Ctrl+O"),this);
	QShortcut* editItemShortcut = new QShortcut(QKeySequence("Ctrl+E"),this);
	QShortcut* saveShortcut = new QShortcut(QKeySequence("Ctrl+S"),this);
	connect(saveShortcut,SIGNAL(activated()),this,SLOT(save()));
	connect(editItemShortcut,SIGNAL(activated()),this,SLOT(editCurrentItem()));
	connect(chooseDirectoryShortcut,SIGNAL(activated()),this,SLOT(chooseDirectory()));
	connect(m_listWidget,SIGNAL(currentItemChanged(QListWidgetItem*,QListWidgetItem*)),this,SLOT(selectFile(QListWidgetItem*)));
}

MW::~MW()
{
}

void MW::chooseDirectory()
{
	QString dirPath = QFileDialog::getExistingDirectory();
	m_workingDir.setPath(dirPath);
	reread();
}

void MW::reread()
{
	QList<QString> fileNames = m_workingDir.entryList(QList<QString>() << "*.png" << "*.jpg");
	m_files.clear();
	for (int i = 0; i < fileNames.size(); i++) {
		File* file = new File(m_workingDir.absoluteFilePath(fileNames.at(i)));
		m_files << file;
		m_listWidget->addItem(file);
	}

}

void MW::editCurrentItem()
{
	if (m_listWidget->selectedItems().size()) {
		QListWidgetItem* curItem = m_listWidget->selectedItems().first();
		File* file = dynamic_cast<File*>(curItem);
		m_listWidget->openPersistentEditor(file);
	}
}

void MW::selectFile(QListWidgetItem *item)
{
	File* file = dynamic_cast<File*>(item);
	m_imageLabel->setPixmap(file->image());
}

void MW::save()
{
	for (int i = 0; i < m_listWidget->count(); i++) {
		File* file = dynamic_cast<File*>(m_listWidget->item(i));
		file->saveWithNewName();
	}
}
