#ifndef MW_H
#define MW_H
#include <QDir>
#include <QFileDialog>
#include <QShortcut>
#include <QMainWindow>
#include <QGraphicsView>
#include <QLineEdit>
#include <QListWidget>
#include <QList>
#include <QHBoxLayout>
#include <QGraphicsItem>
#include <QLabel>

#include <QSizePolicy>
#include "file.h"
class MW : public QMainWindow
{
	Q_OBJECT

public:
	explicit MW(QWidget *parent = 0);
	~MW();

public slots:
	void chooseDirectory();
	void reread();
	void editCurrentItem();
	void selectFile(QListWidgetItem* item);
	void save();
private:
	QLabel* m_imageLabel;
	QListWidget* m_listWidget;
	QLineEdit* m_lineEdit;
	QDir m_workingDir;
	QList<File *> m_files;
};

#endif // MW_H
