#-------------------------------------------------
#
# Project created by QtCreator 2015-01-26T19:05:30
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QRenamer
TEMPLATE = app


SOURCES += main.cpp\
        mw.cpp \
    file.cpp

HEADERS  += mw.h \
    file.h

FORMS    += mw.ui
